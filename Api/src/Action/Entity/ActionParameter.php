<?php

namespace Mush\Action\Entity;

interface ActionParameter
{
    public function getClassName(): string;
}
