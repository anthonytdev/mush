<?php

namespace Mush\Disease\Enum;

class DiseaseEnum
{
    public const ACID_REFLUX = 'acid_reflux';
    public const FOOD_POISONING = 'food_poisoning';
    public const TAPEWORM = 'tapeworm';
    public const GASTROENTERITIS = 'gastroenteritis';
}
