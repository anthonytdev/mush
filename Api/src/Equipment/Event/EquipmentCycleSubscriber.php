<?php

namespace Mush\Equipment\Event;

use Mush\Equipment\Entity\EquipmentMechanic;
use Mush\Equipment\Service\EquipmentCycleHandlerServiceInterface;
use Mush\Equipment\Service\GameEquipmentServiceInterface;
use Mush\Status\Entity\Status;
use Mush\Status\Event\StatusCycleEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EquipmentCycleSubscriber implements EventSubscriberInterface
{
    private EventDispatcherInterface $eventDispatcher;
    private EquipmentCycleHandlerServiceInterface $equipmentCycleHandler;
    private GameEquipmentServiceInterface $gameEquipmentService;

    public function __construct(
        GameEquipmentServiceInterface $gameEquipmentService,
        EquipmentCycleHandlerServiceInterface $equipmentCycleHandler,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->gameEquipmentService = $gameEquipmentService;
        $this->eventDispatcher = $eventDispatcher;
        $this->equipmentCycleHandler = $equipmentCycleHandler;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            EquipmentCycleEvent::EQUIPMENT_NEW_CYCLE => 'onNewCycle',
            EquipmentCycleEvent::EQUIPMENT_NEW_DAY => 'onNewDay',
        ];
    }

    public function onNewCycle(EquipmentCycleEvent $event): void
    {
        $equipment = $event->getGameEquipment();

        /** @var Status $status */
        foreach ($equipment->getStatuses() as $status) {
            $statusNewCycle = new StatusCycleEvent($status, $equipment, $event->getDaedalus(), $event->getTime());
            $this->eventDispatcher->dispatch($statusNewCycle, StatusCycleEvent::STATUS_NEW_CYCLE);
        }

        /** @var EquipmentMechanic $mechanic */
        foreach ($equipment->getEquipment()->getMechanics() as $mechanic) {
            if ($cycleHandler = $this->equipmentCycleHandler->getEquipmentCycleHandler($mechanic)) {
                $cycleHandler->handleNewCycle($equipment, $event->getDaedalus(), $event->getTime());
            }
        }
    }

    public function onNewDay(EquipmentCycleEvent $event): void
    {
        $equipment = $event->getGameEquipment();

        /** @var Status $status */
        foreach ($equipment->getStatuses() as $status) {
            $statusNewDay = new StatusCycleEvent($status, $equipment, $event->getDaedalus(), $event->getTime());
            $this->eventDispatcher->dispatch($statusNewDay, StatusCycleEvent::STATUS_NEW_DAY);
        }

        /** @var EquipmentMechanic $mechanics */
        foreach ($equipment->getEquipment()->getMechanics() as $mechanics) {
            if ($cycleHandler = $this->equipmentCycleHandler->getEquipmentCycleHandler($mechanics)) {
                $cycleHandler->handleNewDay($equipment, $event->getDaedalus(), $event->getTime());
            }
        }
    }
}
