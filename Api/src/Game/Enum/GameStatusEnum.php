<?php

namespace Mush\Game\Enum;

class GameStatusEnum
{
    public const STARTING = 'starting';
    public const CURRENT = 'in_game';
    public const FINISHED = 'finished';
    public const CLOSED = 'closed';
}
