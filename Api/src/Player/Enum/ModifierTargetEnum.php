<?php

namespace Mush\Player\Enum;

class ModifierTargetEnum
{
    public const ACTION_POINT = 'action_point';
    public const MOVEMENT_POINT = 'movement_point';
    public const HEALTH_POINT = 'health_point';
    public const MORAL_POINT = 'moral_point';
    public const SATIETY = 'satiety';

    public const MAX_ACTION_POINT = 'max_action_point';
    public const MAX_MOVEMENT_POINT = 'max_movement_point';
    public const MAX_HEALTH_POINT = 'max_health_point';
    public const MAX_MORAL_POINT = 'max_moral_point';

    public const PERCENTAGE = 'percentage';
}
