<?php

namespace Mush\Status\Enum;

class PlayerStatusEnum
{
    public const ANTISOCIAL = 'antisocial';
    public const BERZERK = 'berzerk';
    public const BRAINSYNC = 'brainsync';
    public const BURDENED = 'burdened';
    public const DEMORALIZED = 'demoralized';
    public const DIRTY = 'dirty';
    public const DISABLED = 'disabled';
    public const EUREKA_MOMENT = 'eureka_moment';
    public const FIRST_TIME = 'first_time';
    public const FOCUSED = 'focused';
    public const FULL_STOMACH = 'full_stomach';
    public const GAGGED = 'gagged';
    public const GERMAPHOBE = 'germaphobe';
    public const GUARDIAN = 'guardian';
    public const HIGHLY_INACTIVE = 'highly_inactive';
    public const HYPERACTIVE = 'hyperactive';
    public const IMMUNIZED = 'immunized';
    public const INACTIVE = 'inactive';
    public const LOST = 'lost';
    public const LYING_DOWN = 'lying_down';
    public const MULTI_TEAMSTER = 'multi_teamster';
    public const MUSH = 'mush';
    public const OUTCAST = 'outcast';
    public const PACIFIST = 'pacifist';
    public const PREGNANT = 'pregnant';
    public const SPORES = 'spores';
    public const STARVING = 'starving';
    public const STUCK_IN_THE_SHIP = 'stuck_in_the_ship';
    public const SUICIDAL = 'suicidal';
    public const DRUG_EATEN = 'drug_eaten';
}
